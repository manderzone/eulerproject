var limit = 10001;
var primes_found = 0;

function find_prime() {
	var curr = 2;
	while (primes_found !== limit) {
		console.log('curr', curr)
		if (is_prime(curr)) {
			primes_found += 1;
		}
		if (primes_found !== limit) {
			curr += 1;
		}
	}
	return curr;
}

function is_prime(number_to_check) {
	for (var i = 2; i < number_to_check; i++) {
		if (number_to_check  % i === 0) {
			return false;
		}
	}
	return true;
}

console.log('find prime', find_prime())
var MAX = 4000000;
var FIBONACCI = [];
var first_value = 0;
var second_value = 0;
var result = 0;
var FIB_TO_SUM= []
var i = 1

function populate_fibonacci() {
	while (true) {
		if (FIBONACCI.length) {
			if (FIBONACCI.length > 1) {
				first_value = FIBONACCI[i - 3];
				second_value = FIBONACCI[i - 2];
			}
			else {
				second_value = i;
			}
      result = first_value + second_value;
      if (result >= MAX) {
      	break;
      }
      FIBONACCI.push(result)
      if (result % 2 === 0) {
				FIB_TO_SUM.push(result);
      }
		}
		else {
			FIBONACCI.push(i)
		}
		i++;
	}
}

function sum_fibonacci() {
	var sum = FIB_TO_SUM.reduce(function(a, b) {
	  return a + b;
	}, 0);
}

populate_fibonacci();
sum_fibonacci();

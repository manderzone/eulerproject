var largest_three_number = 999;
var num1 = largest_three_number;
var num2 = largest_three_number;
var largest_palindrome_found = 0;

var largest_palindrome = find_palindrome();
console.log('largest_palindrome_found', largest_palindrome)

function find_palindrome() {
	var sum = 0;
	for (var i = num1; i > 0; i--) {
		for (var j = num2; j > 0; j--) {
			sum = i * j;
			if (is_palindrome(sum) && sum > largest_palindrome_found) {
				largest_palindrome_found = sum;
			}
		} 
	}
	return largest_palindrome_found;
}

function is_palindrome(number) {
	var splitted_number = split_number(number);
	var first = splitted_number.first;
	var second = splitted_number.second;
	if (first === reverse(second)) {
		return true;
	}
	else {
		return false;
	}
}

function reverse(number){
    return number.split("").reverse().join("");
}

function split_number(number) {
	var first = [];
	var second = [];
	var numberString = number.toString();
	var length = (numberString.length / 2);
	for (var i = 0; i < length; i++) {
		first.push(numberString[i])
	}
	for (var i = length; i < numberString.length; i++) {
		second.push(numberString[i]);
	}
	return {
		first: first.toString().split(',').join(''),
		second: second.toString().split(',').join('')
	}
}


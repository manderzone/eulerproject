var MAX = 1000;

function get_sum_of_multiples() {
	var multiples = [];
	var sum = 0;
	for (var i = 3; i < MAX; i++) {
		if (i % 3 === 0 || i % 5 === 0) {
			sum += i;
	  }
	}
}

get_sum_of_multiples();

var length = 20;

console.log('num', find_smallest_divide())

function find_smallest_divide() {
	var num = 1;
	var smallest_divide_found = false;
	while (!smallest_divide_found) {
		if (!can_be_divided_by_all(num)) {
			num += 1;
		}
		else {
			return num;
		}
	}
}

function can_be_divided_by_all(num) {
	for (var i = 1; i <= length; i++) {
		if (num % i !== 0) {
			return false;
		}
	}
	return true;
}
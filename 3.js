
// börja på primtal 2
// kolla om 13195 kan delas på två
// om inte => HITTANÄSTAPRIMTAL

// HITTANÄSTAPRIMTAL
// ta in nuvarande primtal
// öka med ett
// loopa igenom alla tal och se om det bara går att dela med 1 eller sig självt
// om inte => öka med ett
// om => returnera


var curr_prime = 2;
var number = 600851475143;
var prime_factors = [];

while (number/curr_prime !== 1) {
	if (number % curr_prime !== 0) {
		curr_prime = find_next_prime(curr_prime);
	}
	else {
		prime_factors.push(curr_prime);
		number = number / curr_prime;
	}
}

prime_factors.push(curr_prime);
console.log('prime factors: ', prime_factors)

function find_next_prime(curr_prime) {
	curr = curr_prime + 1;
	found_prime = null;
	while (!found_prime) {
	  if (is_prime(curr)) {
			console.log('found prime', curr)
	  	found_prime = curr;
	  }
	  else {
			curr += 1;
		}
  }
  return found_prime;
}

function is_prime(number_to_check) {
	for (var i = 2; i < number_to_check; i++) {
		if (number_to_check  % i === 0) {
			return false;
		}
	}
	return true;
}
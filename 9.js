var triangelFound = false;
var sum = 0;

var first = 1;
var second = 2;
var third = 997;

var orgf = 1;
var orgs = 2; 
var orgt = 997;

var first_turn = true;

while(!triangelFound) {
	while(!triangelFound && second !== orgt) {
		triangelFound = check_pythagoras(first, second, third);
		if (second >= third) {
			first += 1;
			orgs += 1;
			second = orgs;
			orgt -= 2;
			third = orgt;
		}
		else if (!triangelFound) {	
			second += 1;
			third -= 1;
		}
	}
}

console.log(first * second * third);

function check_pythagoras(first, second, third) {
	if ((first * first) + (second * second) === (third * third)) {
		return true;
	}
	return false;
}

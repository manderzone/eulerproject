var length = 100;

var sum_of_squares = 0;
var squares_of_sum = 0;


function summarize() {
	var sum = 0;
	for (var i = 1; i <= length; i++) {
		sum_of_squares += (i*i);
		squares_of_sum += i;
	}
	squares_of_sum = squares_of_sum * squares_of_sum;
}

function get_diff() {
	return squares_of_sum - sum_of_squares;
}

summarize();
console.log(get_diff());
